import Sequelize from 'sequelize';

const defaultResponse = (data, statusCode = 200) => ({
  data,
  statusCode,
});

const errorResponse = (message, statusCode = 400) => defaultResponse({
  error: message,
}, statusCode);

class TasksController {
  constructor(tasks) {
    this.tasks = tasks;
  }

  getAll(params) {
    const offsetint = parseInt(params.offset ? params.offset : 0, 10);
    const queryParams = {
      offset: offsetint,
      limit: 10,
      where: {
        name: {
          [Sequelize.Op.like]: `${params.name ? params.name : ''}%`,
        },
      },
    };
    return this.tasks.findAll(queryParams)
      .then(result => defaultResponse(result))
      .catch(err => errorResponse(err.message));
  }

  getById(params) {
    return this.tasks.findOne({ where: params })
      .then(result => defaultResponse(result))
      .catch(err => errorResponse(err.message));
  }

  add(data) {
    return this.tasks.create(data)
      .then(result => defaultResponse(result, 201))
      .catch(err => errorResponse(err.message, 422));
  }

  update(data, params) {
    return this.tasks.update(data, { where: params })
      .then(result => defaultResponse(result))
      .catch(err => errorResponse(err.message, 422));
  }

  delete(params) {
    return this.tasks.destroy({ where: params })
      .then(result => defaultResponse(result, 204))
      .catch(err => errorResponse(err.message, 422));
  }

  getTotalTasks(params) {
    const filter = {
      where: {
        name: {
          [Sequelize.Op.like]: `${params.name ? params.name : ''}%`,
        },
      },
    };

    return this.tasks.count(filter)
      .then(result => defaultResponse(result))
      .catch(err => errorResponse(err.message));
  }
}

export default TasksController;
