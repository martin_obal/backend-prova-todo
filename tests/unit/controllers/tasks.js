import TasksController from '../../../controllers/tasks';

describe('Controllers: Tasks', () => {
  describe('Get task by ID: getById()', () => {
    it('should return a task by its Id', () => {
      const tasks = {
        findOne: td.function(),
      };

      const expectedResponse = [{
        id: 99,
        name: 'Default Task',
        created_at: '2019-03-09T21:34:55.692Z',
        updated_at: '2019-03-09T21:34:55.692Z',
      }];

      td.when(tasks.findOne({ where: { id: 99 } })).thenResolve(expectedResponse);

      const tasksController = new TasksController(tasks);
      return tasksController.getById({ id: 99 })
        .then(response => expect(response.data).to.be.eql(expectedResponse));
    });
  });

  describe('Create a task: add()', () => {
    it('should create a task', () => {
      const tasks = {
        create: td.function(),
      };

      const requestBody = {
        name: 'New Task',
      };

      const expectedResponse = [{
        id: 101,
        name: 'New Task',
        created_at: '2019-03-09T21:34:55.692Z',
        updated_at: '2019-03-09T21:34:55.692Z',
      }];

      td.when(tasks.create(requestBody)).thenResolve(expectedResponse);

      const tasksController = new TasksController(tasks);
      return tasksController.add(requestBody)
        .then((response) => {
          expect(response.data).to.be.eql(expectedResponse);
          expect(response.statusCode).to.be.eql(201);
        });
    });
  });

  describe('Update a task: update()', () => {
    it('should update a task', () => {
      const tasks = {
        update: td.function(),
      };

      const requestBody = {
        id: 99,
        name: 'Updated Task',
      };

      const expectedResponse = [{
        id: 99,
        name: 'Updated Task',
        created_at: '2019-03-09T21:34:55.692Z',
        updated_at: '2019-03-09T21:34:55.692Z',
      }];

      td.when(tasks.update(requestBody, { where: { id: 99 } })).thenResolve(expectedResponse);

      const tasksController = new TasksController(tasks);
      return tasksController.update(requestBody, { id: 99 })
        .then(response => expect(response.data).to.be.eql(expectedResponse));
    });
  });

  describe('Delete a task: delete()', () => {
    it('should delete a task', () => {
      const tasks = {
        destroy: td.function(),
      };

      td.when(tasks.destroy({ where: { id: 99 } })).thenResolve({});

      const tasksController = new TasksController(tasks);
      return tasksController.delete({ id: 99 })
        .then(response => expect(response.statusCode).to.be.eql(204));
    });
  });
});
