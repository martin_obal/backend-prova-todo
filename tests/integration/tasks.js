describe('Routes tasks', () => {
  const tasks = app.datasource.models.tasks;

  const defaultTask = {
    id: 99,
    name: 'Default Task',
  };
  const searchTask = {
    id: 150,
    name: 'Specific Task',
  };

  beforeEach((done) => {
    tasks
      .destroy({ where: {} })
      .then(() => {
        tasks.create(defaultTask);
        tasks.create(searchTask);
      })
      .then(() => done());
  });

  describe('Route GET /tasks/{offset}', () => {
    it('should return a list of tasks', (done) => {
      request
        .get('/tasks/0')
        .end((err, res) => {
          expect(res.body[0].id).to.be.eql(defaultTask.id);
          expect(res.body[0].name).to.be.eql(defaultTask.name);

          done(err);
        });
    });
  });

  describe('Route GET /tasks/{offset}/{name}', () => {
    it('should return a list of tasks filtered by a name', (done) => {
      request
        .get('/tasks/0/Speci')
        .end((err, res) => {
          expect(res.body[0].id).to.be.eql(searchTask.id);
          expect(res.body[0].name).to.be.eql(searchTask.name);

          done(err);
        });
    });
  });

  describe('Route GET /task/{id}', () => {
    it('should return a task', (done) => {
      request
        .get('/task/99')
        .end((err, res) => {
          expect(res.body.id).to.be.eql(defaultTask.id);
          expect(res.body.name).to.be.eql(defaultTask.name);

          done(err);
        });
    });
  });

  describe('Route POST /task', () => {
    it('should create a task', (done) => {
      const task = {
        id: 101,
        name: 'New Task',
      };

      request
        .post('/task')
        .send(task)
        .end((err, res) => {
          expect(res.body.id).to.be.eql(task.id);
          expect(res.body.name).to.be.eql(task.name);

          done(err);
        });
    });
  });

  describe('Route PUT /tasks/{id}', () => {
    it('should update a task', (done) => {
      const updatedTask = {
        id: 99,
        name: 'Updated Task',
      };

      request
        .put('/task/99')
        .send(updatedTask)
        .end((err, res) => {
          expect(res.body).to.be.eql([1]);

          done(err);
        });
    });
  });

  describe('Route DELETE /tasks/{id}', () => {
    it('should delete a task', (done) => {
      request
        .delete('/task/99')
        .end((err, res) => {
          expect(res.statusCode).to.be.eql(204);

          done(err);
        });
    });
  });
});
