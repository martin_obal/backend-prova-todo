import TasksController from '../controllers/tasks';

export default (app) => {
  const tasksController = new TasksController(app.datasource.models.tasks);

  app.route('/tasks/:offset/:name*?')
    .get((req, res) => {
      tasksController.getAll(req.params)
        .then((response) => {
          res.status = response.statusCode;
          res.json(response.data);
        });
    });

  app.route('/task')
    .post((req, res) => {
      tasksController.add(req.body)
        .then((response) => {
          res.status = response.statusCode;
          res.json(response.data);
        });
    });

  app.route('/task/:id')
    .get((req, res) => {
      tasksController.getById(req.params)
        .then((response) => {
          res.status = response.statusCode;
          res.json(response.data);
        });
    })
    .put((req, res) => {
      tasksController.update(req.body, req.params)
        .then((response) => {
          res.status = response.statusCode;
          res.json(response.data);
        });
    })
    .delete((req, res) => {
      tasksController.delete(req.params)
        .then((response) => {
          res.sendStatus(response.statusCode);
        });
    });

  app.route('/totalTasks/:name*?')
    .get((req, res) => {
      tasksController.getTotalTasks(req.params)
        .then((response) => {
          res.status = response.statusCode;
          res.json(response.data);
        });
    });
};
