## Aplicação

O projeto backend-prova-todo é uma aplicação que gerencia tarefas (tasks) de uma lista.

---

## Requisitos

1. Necessário instalar a biblioteca cross-env como global: npm install -g cross-env.
*Caso queira rodar o lint da aplicação (npm run lint), é necessário rodar o comando: npm install -g eslint.*
2. Necessário ter um servidor de MySql rodando com as databases de acordo com os ambientes: dev_tasks, test_tasks, prod_tasks
3. Em cada um dos ambientes é necessário ter uma tabela chamada tasks com os campos:
  3.1. id: INT (PK, Not Null, Auto Increment)
  3.2. name: VARCHAR(200) (Not Null)