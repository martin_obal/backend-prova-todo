import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import config from './config/config';
import datasource from './config/datasource';
import tasksRouter from './routes/tasks';

const app = express();

app.use(cors());
app.set('port', 8001);
app.use(bodyParser.json());

app.datasource = datasource(config);
tasksRouter(app);

export default app;
